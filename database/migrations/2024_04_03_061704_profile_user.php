<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        //
        Schema::create('account_user', function (Blueprint $table) {
            $table->id('user_id');
            $table->string('user_id_attr');
            $table->string('role_user');
            $table->string('nama_user');
            $table->string('city');
            $table->string('detail_alamat_user');
            $table->string('kode_pos_user');
            $table->string('email_user')->unique();
            $table->string('password');
            $table->string('no_telp_user');
            $table->string('photo_user')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('account_user');
    }
};
