<?php

namespace App\Models;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable as AuthenticatableTrait;

class AccountUser extends Model implements Authenticatable
{
    use AuthenticatableTrait;
    protected $table = 'account_user';

    protected $primaryKey = 'user_id';

    protected $fillable = [
        'user_id_attr',
        'role_user',
        'nama_user',
        'city',
        'detail_alamat_user',
        'kode_pos_user',
        'email_user',
        'password',
        'no_telp_user',
        'photo_user',
    ];
    // public function rankgamifikasi()
    // {
    //     return $this->belongsTo(RankGamifikasi::class, 'rank_gamifikasi_id', 'rank_gamifikasi_id');
    // }
    // public function city()
    // {
    //     return $this->belongsTo(City::class, 'city_id', 'city_id');
    // }

    // public function forums()
    // {
    //     return $this->hasMany(Forums::class, 'email_user', 'email_user');
    // }
    // public function notifications()
    // {
    //     return $this->hasMany(Notification::class, 'email_user', 'email_user');
    // }
    // public function reportForum()
    // {
    //     return $this->hasMany(ReportForum::class, 'email_user', 'email_user');
    // }

    // public function forumResponses()
    // {
    //     return $this->hasMany(ForumResponse::class, 'email_user', 'email_user');
    // }

    // public function reportForumResponse()
    // {
    //     return $this->hasMany(ReportForumResponse::class, 'email_user', 'email_user');
    // }

    // public function forumLikes()
    // {
    //     return $this->hasMany(ForumLikesCount::class, 'email_user', 'email_user');
    // }
    // public function forumResponseLikes()
    // {
    //     return $this->hasMany(ForumResponseLikesCount::class, 'email_user', 'email_user');
    // }
    // public function badgeUser()
    // {
    //     return $this->hasMany(BadgeUser::class, 'rank_account_user_id', 'account_user_id');
    // }
    // public function historyGamifikasi()
    // {
    //     return $this->hasMany(HistoryGamifikasi::class, 'rank_account_user_id', 'account_user_id');
    // }
    // public function preloved()
    // {
    //     return $this->hasMany(Preloved::class, 'user_id', 'rank_account_user_id');
    // }

    // public function kurir()
    // {
    //     return $this->hasMany(AccountUser::class, 'kurir_id', 'rank_account_user_id');
    // }
}
