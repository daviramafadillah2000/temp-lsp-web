<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Mail\ConfirmAccountMail;
use App\Models\Province;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Models\AccountUser;
use App\Models\City;
use App\Models\RankGamifikasi;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

use App\Mail\DemoMail;
use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail as FacadesMail;
use Illuminate\Support\Facades\Storage;

class AccountUserController
{
    public function register(Request $request)
    {
        $request->validate([
            'nama_user' => 'required|string',
            'city' => 'required',
            'detail_alamat_user' => 'required|string',
            'kode_pos_user' => 'required|numeric',
            'email_user' => 'required|email|unique:account_user,email_user',
            'password' => 'required|string|min:8',
            'no_telp_user' => 'required|numeric',
            'photo_user' => 'required|image|mimes:jpeg,jpg,png,webp|max:2048',
        ], [
            'email_user.unique' => 'Email sudah terdaftar.',
            'photo_user.image' => 'File harus berupa gambar.',
            'photo_user.mimes' => 'File harus berupa jpeg, jpg, png, atau webp.',
            'photo_user.max' => 'Ukuran file maksimal 2048 kilobyte.',
        ]);

        if ($request->hasFile('photo_user')) {
            $photoUser = $request->file('photo_user');
            $originalName = $photoUser->getClientOriginalName(); // Nama asli file termasuk ekstensi
            // dd($originalName);
            $filename = time() . '_' . $originalName;
            $photoUser->storeAs('photo_user', $filename);
            $namaPhotoUser = $filename;
        } else {
            $namaPhotoUser = null;
        }


        $user = AccountUser::create([
            'role_user' => "admin",
            'nama_user' => $request->nama_user,
            'city' => $request->city,
            'detail_alamat_user' => $request->detail_alamat_user,
            'kode_pos_user' => $request->kode_pos_user,
            'email_user' => $request->email_user,
            'password' => Hash::make($request->password),
            'no_telp_user' => $request->no_telp_user,
            'photo_user' => $namaPhotoUser,
        ]);

        return redirect()->route('home')->with('success', 'Akun Berhasil Dibuat!');
    }

    public function login(Request $request)
    {
        $request->validate(['email_user' => 'required|string', 'password' => 'required|string']);

        if (Auth::attempt(['email_user' => $request->email_user, 'password' => $request->password])) {
            $user = Auth::user();
            if ($user->role_user == "pelanggan" && $user->status_konfirmasi_account_user == "iya") {
                return redirect()->back()->with('success', 'Berhasil Login');
            } elseif ($user->role_user == "pelanggan" && $user->status_konfirmasi_account_user == "tidak") {
                Auth::logout();
                return redirect()->back()->with('error', 'Silahkan Konfirmasi Akun Anda Melalui Email Yang Telah Dikirimkan Telebih Dahulu');
            } elseif ($user->role_user == "admin") {
                return redirect()->route('admin')->with('success', 'Berhasil Login');
            } elseif ($user->role_user == "kurir") {
                return redirect()->route('inspektor.index')->with('success', 'Berhasil Login');
            }
        }
        Auth::logout();
        return redirect()->route('home')->withErrors(['email_user' => 'Gagal login, Email atau password Anda salah.']);
    }

    public function logout()
    {
        Auth::logout();

        // Redirect to the desired location after logout, for example, the home page
        return redirect()->route('home')->with('success', 'Berhasil Logout');
    }
}
