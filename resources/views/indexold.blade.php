@extends('layouts.main')
@section('contentMain')
    <div class="grid grid-cols-1">
        {{-- Top Hero --}}
        <div id="default-carousel" class="relative w-full" data-carousel="slide">
            <!-- Carousel wrapper -->
            <div class="relative h-20 overflow-hidden md:h-52 lg:h-72">
                @foreach ($banners['items'] as $banner)
                    @foreach ($banners['includes']['Asset'] as $asset)
                        @if ($asset['sys']['id'] === $banner['fields']['banner']['sys']['id'])
                            <div class="hidden duration-1000 ease-in-out" data-carousel-item>
                                <img src="https:{{ $asset['fields']['file']['url'] }}"
                                    class="absolute block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2"
                                    alt="{{ $banner['fields']['alt'] }}">
                            </div>
                        @endif
                    @endforeach
                @endforeach
            </div>
            <!-- Slider indicators -->
            <div class="absolute z-30 flex -translate-x-1/2 bottom-5 left-1/2 space-x-3 rtl:space-x-reverse">
                @foreach ($banners['items'] as $key => $banner)
                    <button type="button" class="w-3 h-3 rounded-full sm:w-4 sm:h-4" aria-current="true"
                        aria-label="Slide {{ $key }}" data-carousel-slide-to="{{ $key }}"></button>
                @endforeach
            </div>
            <!-- Slider controls -->
            <button type="button"
                class="absolute top-0 start-0 z-30 flex items-center justify-center h-full px-2 sm:px-4 cursor-pointer group focus:outline-none"
                data-carousel-prev>
                <span
                    class="inline-flex items-center justify-center w-8 h-8 sm:w-10 sm:h-10 rounded-full bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
                    <svg class="w-3 h-3 sm:w-4 sm:h-4 text-white dark:text-gray-800 rtl:rotate-180" aria-hidden="true"
                        xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 6 10">
                        <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                            d="M5 1 1 5l4 4" />
                    </svg>
                    <span class="sr-only">Previous</span>
                </span>
            </button>
            <button type="button"
                class="absolute top-0 end-0 z-30 flex items-center justify-center h-full px-2 sm:px-4 cursor-pointer group focus:outline-none"
                data-carousel-next>
                <span
                    class="inline-flex items-center justify-center w-8 h-8 sm:w-10 sm:h-10 rounded-full bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
                    <svg class="w-3 h-3 sm:w-4 sm:h-4 text-white dark:text-gray-800 rtl:rotate-180" aria-hidden="true"
                        xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 6 10">
                        <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                            d="m1 9 4-4-4-4" />
                    </svg>
                    <span class="sr-only">Next</span>
                </span>
            </button>
        </div>

        <div>
            <div class="mx-5 lg:max-w-2xl lg:mx-auto py-2 md:py-5">
                <div class="relative">
                    <form action="{{ route('produk.listHalamanProduk') }}" method="GET">
                        <div class="absolute inset-y-0 start-0 flex items-center ps-3 pointer-events-none">
                            <svg class="w-4 text-gray-500" aria-hidden="true" xmlns="http://www.w3.org/2000/svg"
                                fill="none" viewBox="0 0 20 20">
                                <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                    d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z" />
                            </svg>
                        </div>
                        <input type="search" id="default-search" name="nama_produk"
                            class="block w-full p-4 ps-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-custom-purple focus:border-custom-purple"
                            placeholder="Cari Produk" required />
                        <button type="submit"
                            class="text-white absolute end-2.5 bottom-2.5 transition duration-500 ease-in-out bg-custom-purple  focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 hover:bg-white hover:text-black border border-custom-purple">Cari</button>
                    </form>
                </div>
            </div>
        </div>

        <div class="my-2 lg:my-5 sticky top-16 bg-white">
            <div class="flex justify-start ml-5">
                <p class="text-xl font-bold">Produk Terbaru</p>
            </div>
            <div class="flex justify-center px-5">
                <div class="grid grid-cols-6 overflow-y-hidden overflow-x-auto gap-60 lg:gap-60 xl:gap-32 px-5 py-4">
                    @foreach ($produkIndexs as $key => $produkIndex)
                        <div
                            class="bg-white text-start p-2 px-2 rounded-lg shadow-md border border-transparent transition duration-700 ease-in-out w-56 transform hover:scale-105 hover:border-gray-600">
                            <a href="{{ route('produk.detail-produk', ['produk_id' => $produkIndex->produk_id]) }}">
                                <div class="flex flex-col justify-between h-full">
                                    <div class="flex justify-center p-2">
                                        <img src="{{ asset('storage/gambarproduk1/' . $produkIndex->foto_produk1) }}"
                                            class="h-32 md:h-48"
                                            data-src="{{ asset('storage/gambarproduk1/' . $produkIndex->foto_produk1) }}">
                                    </div>
                                    <hr class="h-px mb-2 bg-gray-400 border-0">
                                    <div class="flex-grow">
                                        <p class="text-lg font-semibold mb-1 break-words">
                                            {{ $produkIndex->brand->nama_brand }}
                                        </p>
                                        <p class="text-gray-700 break-words text-xs mb-1 capitalize text-justify">
                                            {{ $produkIndex->nama_produk }} - {{ $produkIndex->warna_produk }}
                                        </p>
                                    </div>

                                    <div class="flex-grow p-1">
                                        <div class="flex-grow grid grid-cols-3">
                                            <p
                                                class="text-xs  text-center flex justify-center items-center h-5 rounded-xl text-white @if ($produkIndex->jenis_kelamin_produk == 'Wanita') bg-red-400 @elseif($produkIndex->jenis_kelamin_produk == 'Pria') bg-blue-400 @elseif($produkIndex->jenis_kelamin_produk == 'Unisex') bg-yellow-300 @endif">
                                                {{ $produkIndex->jenis_kelamin_produk }}</p>
                                            <p
                                                class="text-xs ml-1 h-5 rounded-lg  text-white bg-custom-purple text-center flex justify-center items-center">
                                                {{ $produkIndex->ukuran_produk }}</p>
                                        </div>
                                        <div class="mt-auto">
                                            <p
                                                class="text-{{ $produkIndex->diskon_produk != 0 ? 'red-400' : 'black mb-3' }} break-words text-base font-bold">
                                                Rp. {{ number_format($produkIndex->harga_akhir_produk, 0, ',', '.') }}
                                            </p>
                                        </div>
                                        <div class="">
                                            <p
                                                class="text-gray-700 break-words text-sm font-normal {{ $produkIndex->diskon_produk != 0 ? 'line-through' : 'hidden' }}">
                                                Rp. {{ number_format($produkIndex->harga_produk, 0, ',', '.') }}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>


            <div class="flex justify-center my-2">
                <a href="{{ route('produk.listHalamanProduk') }}"
                    class="max-w-60 flex justify-center items-center border border-custom-purple text-custom-midnight font-bold text-center w-full whitespace-nowrap py-2 px-5 mt-5 rounded-md mb-10 relative overflow-hidden hover:text-white ease-linear duration-500">
                    <div class="z-10 text-xl">Produk Lainnya</div>
                    <span class="custom-ease-in absolute bg-custom-purple top-0 left-0 w-0 h-full"></span>
                </a>
            </div>

        </div>

        <div class="flex justify-start items-start flex-col md:flex-row bg-custom-white md:sticky md:top-16">
            <img src="{{ asset('img/logo/poster-pastiori.jpg') }}" alt="about pastiori" class="md:max-w-96 sticky top-16">
            <div
                class="py-6 min-h-96 bg-custom-maroon flex justify-center items-center flex-col w-full h-full sticky top-16">
                {{-- <p class="uppercase text-3xl text-custom-white font-bold">About Us</p> --}}
                <div class="my-4 text-center">
                    <p class="text-3xl text-custom-white font-light">Pastiori
                        <span class="text-sm text-custom-yellow">by</span> Istana Thrift
                    </p>
                </div>
            </div>
            <div class="flex flex-col p-10 text-custom-white md:max-w-96 w-full h-full bg-custom-purple sticky top-16">
                <span class="text-center text-3xl mb-4 capitalize font-bold">About <span
                        class="text-custom-yellow">Us</span></span>
                <p class="text-center md:text-end text-sm">
                    Pastiori adalah titik temu bagi para pencinta fashion yang menghargai nilai keunikan dan keberlanjutan.
                    Berdiri sebagai platform E-Commerce yang mengkhususkan diri dalam penjualan pakaian Thrift/Preloved,
                    kami
                    berkomitmen untuk menyediakan pengalaman berbelanja yang memuaskan dan bermakna bagi pelanggan kami.
                    Kami
                    percaya bahwa setiap pakaian memiliki cerita, dan melalui Pastiori, kami ingin memperpanjang rentang
                    cerita
                    tersebut dengan menghubungkan pembeli dan penjual dari berbagai latar belakang. Dengan jaminan
                    originalitas
                    dan kesesuaian kondisi produk, kami memastikan setiap transaksi di Pastiori dilakukan dengan integritas
                    dan
                    kepercayaan. Bergabunglah dengan komunitas kami dan temukan keindahan dalam setiap detail fashion yang
                    kami
                    tawarkan. Mari bersama-sama merayakan keunikan dan keberlanjutan dalam dunia fashion melalui Pastiori.
                </p>
            </div>
        </div>

        <div class="py-8 md:py-16 bg-custom-white sticky top-16">
            <div class="flex justify-center">
                <p class="text-xl text-custom-purple md:text-2xl font-bold">Artikel Featured</p>
            </div>
            <div class="flex justify-center m-4">
                <p class="text-base text-custom-purple md:text-lg font-normal text-center">Artikel Menarik & Beragam
                    Informasi
                    Seputar Dunia Thrift
                </p>
            </div>
            <div class="relative overflow-x-auto md:px-10 py-4 my-6">
                <div class="flex flex-row">
                    @php
                        $articlesCollectionByNewest = collect($articles['items'])
                            ->take(7)
                            ->sortByDesc(function ($article) {
                                return strtotime($article['sys']['createdAt']);
                            });
                    @endphp
                    @foreach ($articlesCollectionByNewest as $article)
                        <div
                            class="flex flex-col shadow-md rounded-lg min-w-48 max-w-48 w-48 md:max-w-72 md:min-w-72 md:w-72 mx-2">
                            @foreach ($articles['includes']['Asset'] as $asset)
                                @if ($asset['sys']['id'] === $article['fields']['imageArtikel']['sys']['id'])
                                    <img src="https:{{ $asset['fields']['file']['url'] }}"
                                        class="rounded-t-md aspect-video" alt="{{ $article['fields']['title'] }}">
                                @endif
                            @endforeach
                            <div class="relative flex flex-col p-4 rounded-2xl -mt-3 bg-white h-full">
                                <div
                                    class="font-extrabold w-fit text-custom-purple rounded-md border shadow-lg mb-4 text-center p-1 text-xs">
                                    {{ $article['fields']['categoryArtikel'] }}
                                </div>
                                <div class="font-bold">
                                    {{ $article['fields']['title'] }}
                                </div>
                                <div class="mt-5 flex items-end h-full">
                                    <a href="{{ route('article.show', $article['fields']['slug']) }}"
                                        class="inline-flex text-custom-yellow font-medium rounded-lg text-xs px-3 py-2 bg-transparent ring-2 ring-custom-yellow relative overflow-hidden hover:text-white ease-linear duration-500">
                                        <div class="inline-flex items-center font-bold z-10">
                                            Lihat Artikel
                                            <svg class="w-3 h-3 ms-2.5 rtl:rotate-[270deg]" aria-hidden="true"
                                                xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 18 18">
                                                <path stroke="currentColor" stroke-linecap="round"
                                                    stroke-linejoin="round" stroke-width="2"
                                                    d="M15 11v4.833A1.166 1.166 0 0 1 13.833 17H2.167A1.167 1.167 0 0 1 1 15.833V4.167A1.166 1.166 0 0 1 2.167 3h4.618m4.447-2H17v5.768M9.111 8.889l7.778-7.778" />
                                            </svg>
                                        </div>
                                        <span class="custom-ease-in absolute bg-custom-yellow top-0 left-0 w-0 h-full">
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
