@extends('layouts.main')
@section('contentMain')
    <div class="relative p-4 w-full max-h-full flex justify-center">
        <!-- Modal content -->
        <div class="relative bg-white rounded-lg shadow">
            <!-- Modal header -->
            <div class="flex items-center justify-between p-4 md:p-5 border-b border-custom-purple rounded-t ">
                <h3 class="text-lg font-semibold text-gray-900 dark:text-white">
                    Daftar Akun Baru
                </h3>

            </div>
            <!-- Modal body -->
            <form id="register-form" action="{{ route('account.register') }}" method="POST" class="p-4 md:p-5"
                enctype="multipart/form-data">
                @csrf
                <div class="grid gap-4 mb-4 grid-cols-2">
                    <div class="col-span-2">
                        <label for="nama_user" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Nama
                            Lengkap</label>
                        <input type="text" name="nama_user" id="nama_user"
                            class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-custom-purple focus:border-custom-purple block w-full p-2.5"
                            placeholder="Masukan Nama Anda" required>
                    </div>

                    <div class="col-span-2">
                        <label for="email_user"
                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Email</label>
                        <input type="email" name="email_user" id="email_user_register"
                            class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-custom-purple focus:border-custom-purple block w-full p-2.5"
                            placeholder="Masukan Email Anda" required>
                    </div>

                    <div class="col-span-2">
                        <label for="password"
                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Password</label>
                        <input type="password" name="password" id="password_register"
                            class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-custom-purple focus:border-custom-purple block w-full p-2.5"
                            placeholder="Masukan Password Anda" required minlength="8">
                    </div>

                    <div class="col-span-2">
                        <label for="city"
                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Kota</label>
                        <input type="text" name="city" id="kota_user"
                            class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-custom-purple focus:border-custom-purple block w-full p-2.5"
                            placeholder="Masukan Kota Anda" required>
                    </div>

                    <div class="col-span-2">
                        <label for="detail_alamat_user"
                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Alamat Detail</label>
                        <input type="text" name="detail_alamat_user" id="detail_alamat_user"
                            class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-custom-purple focus:border-custom-purple block w-full p-2.5"
                            placeholder="Masukan Detail Alamat Lengkap Anda" required>
                    </div>

                    <div class="col-span-2 sm:col-span-1">
                        <label for="kode_pos_user"
                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Kodepos</label>
                        <input type="text" name="kode_pos_user" id="kode_pos_user"
                            class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-custom-purple focus:border-custom-purple block w-full p-2.5"
                            placeholder="Masukan Kode Pos Anda" required pattern="[0-9]+">
                    </div>
                    <div class="col-span-2 sm:col-span-1">
                        <label for="no_telp_user" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Nomor
                            Telepon</label>
                        <input type="tel" name="no_telp_user" id="no_telp_user"
                            class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-custom-purple focus:border-custom-purple block w-full p-2.5"
                            placeholder="Masukan Nomor Telepon Anda" required pattern="[0-9]+">
                    </div>

                    <div class="col-span-2">
                        <label class="block mb-2 text-sm font-medium text-gray-900 " for="file_input">Upload Photo
                            User</label>
                        <!-- Preview image container -->
                        <div id="image_preview_create" class="mt-4"></div>

                        <input
                            class="block w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer bg-gray-50 focus:outline-none "
                            aria-describedby="file_input_help" id="file_input" type="file" name="photo_user" required
                            title="Klik untuk Upload" accept=".png, .jpg, .jpeg" onchange="previewImageCreate(this)">
                        <p class="mt-1 text-sm text-gray-500 " id="file_input_help">PNG Atau JPG
                            (MAX.
                            800x400px).</p>
                    </div>

                </div>
                <button type="submit"
                    class="text-white w-full inline-flex items-center justify-center bg-custom-purple hover:bg-custom-midnight focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5">
                    <span class="text-center">Daftar Akun Baru</span>
                </button>
            </form>
        </div>
    </div>

    <script>
        function previewImageCreate(input) {
            const previewContainer = document.getElementById('image_preview_create');
            previewContainer.innerHTML = ''; // Clear previous preview

            const file = input.files[0];

            if (file) {
                const reader = new FileReader();

                reader.onload = function(e) {
                    const imgElement = document.createElement('img');
                    imgElement.src = e.target.result;
                    imgElement.classList.add('max-w-xs', 'max-h-xs', 'mx-auto');
                    previewContainer.appendChild(imgElement);
                }

                reader.readAsDataURL(file);
            }
        }
    </script>
@endsection

{{-- @extends('layouts.admin')
@section('contentAdmin')
@endsection --}}
