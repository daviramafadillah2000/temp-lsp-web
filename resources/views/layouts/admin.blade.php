@extends('layouts.app')
@section('content')
    <nav class="fixed top-0 z-50 w-full bg-custom-yellow border-b border-gray-200 dark:bg-gray-800 dark:border-gray-700">
        <div class="px-3 py-3 lg:px-5 lg:pl-3">
            <div class="flex items-center justify-between">
                <div class="flex items-center justify-start rtl:justify-end">
                    <button data-drawer-target="logo-sidebar" data-drawer-toggle="logo-sidebar" aria-controls="logo-sidebar"
                        type="button"
                        class="inline-flex items-center p-2 text-sm text-gray-500 rounded-lg sm:hidden hover:bg-custom-yellow hover:text-custom-white focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:focus:ring-gray-600">
                        <span class="sr-only">Open sidebar</span>
                        <svg class="w-6 h-6" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20"
                            xmlns="http://www.w3.org/2000/svg">
                            <path clip-rule="evenodd" fill-rule="evenodd"
                                d="M2 4.75A.75.75 0 012.75 4h14.5a.75.75 0 010 1.5H2.75A.75.75 0 012 4.75zm0 10.5a.75.75 0 01.75-.75h7.5a.75.75 0 010 1.5h-7.5a.75.75 0 01-.75-.75zM2 10a.75.75 0 01.75-.75h14.5a.75.75 0 010 1.5H2.75A.75.75 0 012 10z">
                            </path>
                        </svg>
                    </button>
                    <a href="{{ url('/') }}" class="flex items-center space-x-3 rtl:space-x-reverse">
                        <img src="{{ asset('img/logo/logopastiori.png') }}" class="h-12" alt="Pastiori Logo" />
                    </a>
                </div>
                <div class="flex items-center">
                    <div class="flex items-center ms-3">
                        <div>
                            <button type="button"
                                class="flex text-sm bg-gray-800 rounded-full focus:ring-4 focus:ring-gray-300 dark:focus:ring-gray-600"
                                aria-expanded="false" data-dropdown-toggle="dropdown-user">
                                <span class="sr-only">Open user menu</span>
                                <img class="w-8 h-8 rounded-full"
                                    src="{{ asset('img/photoprofile/defaultPhotoProfile.jpg') }}" alt="user photo">
                            </button>
                        </div>
                        <div class="z-50 hidden my-4 text-base list-none bg-white divide-y divide-gray-100 rounded shadow dark:bg-gray-700 dark:divide-gray-600"
                            id="dropdown-user">
                            <div class="px-4 py-3" role="none">
                                <p class="text-sm text-gray-900 dark:text-white" role="none">
                                    {{-- Hallo, {{ Auth::user()->nama_user }} --}}
                                </p>
                                <p class="text-sm font-medium text-gray-900 truncate" role="none">
                                    {{-- {{ Auth::user()->email_user }} --}}
                                </p>
                            </div>
                            <ul class="py-1" role="none">
                                <li>
                                    <a href="{{ url('/admin') }}"
                                        class="block px-4 py-2 text-sm text-custom-midnight hover:bg-custom-yellow hover:text-custom-white dark:hover:bg-gray-600 dark:hover:text-white"
                                        role="menuitem">Dashboard</a>
                                </li>
                                <li>
                                    <form method="POST">
                                        @csrf
                                        <button type="submit"
                                            class="flex items-center justify-center w-full p-2 transition duration-500 ease-in-out text-custom-white bg-custom-pink hover:bg-custom-red focus:ring-custom-pink focus:outline-none">
                                            <span class="material-symbols-outlined">
                                                Logout
                                            </span>
                                            <b class="text-center">Logout</b>
                                        </button>
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>

    <aside id="logo-sidebar"
        class="fixed top-0 left-0 z-40 w-64 h-screen pt-20 transition-transform -translate-x-full bg-custom-white border-r border-gray-200 sm:translate-x-0"
        aria-label="Sidebar">
        <div class="h-full px-3 pb-4 overflow-y-auto bg-custom-white dark:bg-gray-800">
            <ul class="space-y-2 font-medium border-b-2">
                <li>
                    <a href="{{ url('/admin') }}"
                        class="flex items-center p-2 text-custom-midnight rounded-lg hover:bg-custom-yellow hover:text-custom-white">
                        <span class="material-symbols-outlined">
                            dashboard
                        </span>
                        <span class="ms-3">Dashboard</span>
                    </a>
                </li>
                <li>
                    <button type="button"
                        class="flex items-center w-full p-2 text-base text-custom-midnight transition duration-75 rounded-lg hover:bg-custom-yellow hover:text-custom-white"
                        aria-controls="dropdown-manajemen-produk" data-collapse-toggle="dropdown-manajemen-produk">
                        <svg class="w-6 h-6 text-gray-800 dark:text-white" aria-hidden="true"
                            xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="none"
                            viewBox="0 0 24 24">
                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M6 12c.263 0 .524-.06.767-.175a2 2 0 0 0 .65-.491c.186-.21.333-.46.433-.734.1-.274.15-.568.15-.864a2.4 2.4 0 0 0 .586 1.591c.375.422.884.659 1.414.659.53 0 1.04-.237 1.414-.659A2.4 2.4 0 0 0 12 9.736a2.4 2.4 0 0 0 .586 1.591c.375.422.884.659 1.414.659.53 0 1.04-.237 1.414-.659A2.4 2.4 0 0 0 16 9.736c0 .295.052.588.152.861s.248.521.434.73a2 2 0 0 0 .649.488 1.809 1.809 0 0 0 1.53 0 2.03 2.03 0 0 0 .65-.488c.185-.209.332-.457.433-.73.1-.273.152-.566.152-.861 0-.974-1.108-3.85-1.618-5.121A.983.983 0 0 0 17.466 4H6.456a.986.986 0 0 0-.93.645C5.045 5.962 4 8.905 4 9.736c.023.59.241 1.148.611 1.567.37.418.865.667 1.389.697Zm0 0c.328 0 .651-.091.94-.266A2.1 2.1 0 0 0 7.66 11h.681a2.1 2.1 0 0 0 .718.734c.29.175.613.266.942.266.328 0 .651-.091.94-.266.29-.174.537-.427.719-.734h.681a2.1 2.1 0 0 0 .719.734c.289.175.612.266.94.266.329 0 .652-.091.942-.266.29-.174.536-.427.718-.734h.681c.183.307.43.56.719.734.29.174.613.266.941.266a1.819 1.819 0 0 0 1.06-.351M6 12a1.766 1.766 0 0 1-1.163-.476M5 12v7a1 1 0 0 0 1 1h2v-5h3v5h7a1 1 0 0 0 1-1v-7m-5 3v2h2v-2h-2Z" />
                        </svg>

                        <span class="flex-1 ms-3 text-left rtl:text-right whitespace-nowrap">Manajemen Produk</span>

                        <svg class="w-3 h-3 ml-2" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none"
                            viewBox="0 0 10 6">
                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="m1 1 4 4 4-4" />
                        </svg>
                    </button>
                    <ul id="dropdown-manajemen-produk" class="hidden py-2 space-y-2">
                        <li>
                            <a href="#  "
                                class="flex items-center w-full p-2 text-custom-midnight transition duration-75 rounded-lg pl-11 hover:bg-custom-yellow hover:text-custom-white">Manajemen
                                Brand</a>
                        </li>
                        <li>
                            <a href="#"
                                class="flex items-center w-full p-2 text-custom-midnight transition duration-75 rounded-lg pl-11 hover:bg-custom-yellow hover:text-custom-white">Manajemen
                                Jenis Pakaian</a>
                        </li>
                        <li>
                            <a href="#"
                                class="flex items-center w-full p-2 text-custom-midnight transition duration-75 rounded-lg pl-11 hover:bg-custom-yellow hover:text-custom-white">Manajemen
                                Produk Jual</a>
                        </li>
                        <li>
                            <a href="#"
                                class="flex items-center w-full p-2 text-custom-midnight transition duration-75 rounded-lg pl-11 hover:bg-custom-yellow hover:text-custom-white">Manajemen
                                Lelang
                            </a>
                        </li>
                        <li>
                            <a
                                class="flex items-center w-full p-2 text-custom-midnight transition duration-75 rounded-lg pl-11 hover:bg-custom-yellow hover:text-custom-white">Manajemen
                                guide foto pengajuan</a>
                        </li>
                    </ul>
                </li>


                <li>
                    <form method="POST">
                        @csrf
                        <button type="submit"
                            class="flex items-center justify-center w-full p-2 transition duration-500 ease-in-out rounded-lg text-custom-white bg-custom-pink hover:bg-custom-red focus:ring-custom-pink focus:outline-none">
                            <span class="material-symbols-outlined">
                                Logout
                            </span>
                            <b class="text-center">Logout</b>
                        </button>
                    </form>
                </li>
            </ul>
        </div>
    </aside>

    <div class="p-4 sm:ml-64 min-h-screen bg-custom-white">
        <div class="p-4 mt-14">
            @yield('contentAdmin')
        </div>
    </div>
@endsection
