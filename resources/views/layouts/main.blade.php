@extends('layouts.app')
@section('content')
    <div class="">
        <nav class="fixed top-0 w-full z-50  border-gray-200 bg-custom-yellow  py-0 lg:py-1">
            <div class="max-w-screen-xl flex flex-wrap items-center justify-between mx-auto p-4 md:p-2">
                <button data-collapse-toggle="navbar-user" type="button"
                    class="inline-flex items-center p-2 w-10 h-10 justify-center text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 "
                    aria-controls="navbar-user" aria-expanded="false">
                    <span class="sr-only">Open main menu</span>
                    <svg class="w-5 h-5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none"
                        viewBox="0 0 17 14">
                        <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                            d="M1 1h15M1 7h15M1 13h15" />
                    </svg>
                </button>
                <a href="{{ route('home') }}" class="flex items-center space-x-3 rtl:space-x-reverse">
                    <img src="{{ asset('img/logo/logopastiori.png') }}" class="h-12" alt="Flowbite Logo" />
                </a>
                <div class="flex items-center md:order-2 space-x-3 md:space-x-0 rtl:space-x-reverse">
                    @guest
                        <button data-modal-target="authentication-modal" data-modal-toggle="authentication-modal"
                            class="flex items-center gap-2 transition duration-500 ease-in-out text-custom-white bg-custom-purple hover:bg-custom-maroon hover:text-custom-dark-white focus:ring-4 focus:outline-none focus:ring-custom-purple font-medium rounded-lg text-xs px-3 py-2 lg:px-5 text-center"
                            type="button">
                            <span class="material-symbols-outlined hidden lg:block">
                                login
                            </span>
                            <b>Login</b>
                        </button>
                    @else
                    @endguest


                </div>
                <div class="items-center bg-white rounded-lg md:bg-custom-yellow justify-between hidden w-full md:flex md:w-auto md:order-1"
                    id="navbar-user">
                    <ul
                        class="flex flex-col font-medium p-4 md:p-0 lg:mt-2 border rounded-lg md:space-x-8 rtl:space-x-reverse md:flex-row md:mt-0 md:border-0 ">
                        <li>
                            <a href="{{ url('/') }}"
                                class="block py-2 px-3 text-custom-midnight rounded hover:text-white hover:bg-custom-yellow md:bg-transparent md:text-custom-bg-custom-white md:hover:text-white md:p-0 {{ request()->is('/') ? 'text-white bg-custom-yellow' : '' }}"
                                aria-current="page">Home</a>
                        </li>

                        {{-- @endunless --}}
                    </ul>
                </div>
            </div>
        </nav>

        <div class="pt-16 lg:pt-16 min-h-screen">@yield('contentMain')</div>
        <div
            class="flex flex-col md:flex-row justify-center text-custom-yellow pt-4 md:py-8 md:px-40 w-full bg-custom-purple">

            <div class="py-6 flex items-center justify-center">
                <img src="{{ asset('img/logo/logopastiori.png') }}" alt="pastiori logo"
                    class="max-w-24 max-h-12 md:max-w-40 md:max-h-16">
            </div>
            <div class="border-l-2 border-custom-yellow mx-4">

            </div>
            <div class="hidden md:flex md:justify-start py-2 text-sm md:text-lg w-full text-center items-center">
                2024
                Pastiori.
                All rights
                reserved.
            </div>

            <div class="grid grid-flow-row w-full md:ml-16">
                <div
                    class="text-base text-start md:text-end md:text-2xl font-bold capitalize mx-10 md:mx-0 mb-4 border-b-2 border-custom-yellow pl-2 md:pr-2">
                    contact us
                </div>
                <div class="ml-12 md:ml-2 text-xs font-semibold">
                    <div class="flex flex-row items-center md:justify-end mb-2 ">
                        <p class="text-custom-white">
                            081283651588
                        </p>
                        <span class="material-symbols-outlined mr-4 md:ml-4 text-custom-white">
                            call
                        </span>
                    </div>
                    <div class="flex flex-row items-center md:justify-end">
                        <p class="text-custom-white">
                            istanaimport@gmail.com
                        </p>
                        <span class="material-symbols-outlined mr-4 md:ml-4 text-custom-white">
                            mark_as_unread
                        </span>
                    </div>
                </div>
            </div>
            <div class="border-t-2 border-custom-yellow mt-8">

            </div>
            <div
                class="flex md:hidden md:justify-start justify-center py-2 text-sm w-full text-center items-center font-medium">
                2024
                Pastiori.
                All rights
                reserved.
            </div>
        </div>
    </div>


    <!-- Modal Login -->
    <div id="authentication-modal" data-modal-backdrop="static" tabindex="-1" aria-hidden="true"
        class="hidden overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 justify-center items-center w-full md:inset-0 h-[calc(100%-1rem)] max-h-full">
        <div class="relative p-4 w-full max-w-md max-h-full">
            <!-- Modal content -->
            <div class="relative bg-white rounded-lg shadow dark:bg-gray-700">
                <!-- Modal header -->
                <div
                    class="flex items-center justify-between p-4 md:p-5 border-b border-custom-purple rounded-t dark:border-gray-600">
                    <h3 class="text-xl font-semibold text-gray-900 dark:text-white">
                        Login Akun Pengguna
                    </h3>
                    <button type="button"
                        class="end-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 ms-auto inline-flex justify-center items-center"
                        data-modal-hide="authentication-modal">
                        <svg class="w-3 h-3" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none"
                            viewBox="0 0 14 14">
                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6" />
                        </svg>
                        <span class="sr-only">Close modal</span>
                    </button>
                </div>
                <!-- Modal body -->
                <div class="p-4 md:p-5">
                    <form class="space-y-4" method="POST">
                        @csrf
                        <div>
                            <label for="email_user" class="block mb-2 text-sm font-medium text-gray-900 ">Email</label>
                            <input type="email_user" name="email_user" id="email_user"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-custom-purple focus:border-custom-purple block w-full p-2.5"
                                placeholder="Masukan Email Anda" required
                                oninvalid="setCustomValidity('Masukan Email Akun Anda Dengan Format Email Yang Benar')" />
                        </div>
                        <div>
                            <label for="password" class="block mb-2 text-sm font-medium text-gray-900 ">
                                password</label>
                            <input type="password" name="password" id="password" placeholder="••••••••"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-custom-purple focus:border-custom-purple block w-full p-2.5"
                                required oninvalid="setCustomValidity('Masukan Password Akun Anda')" />
                        </div>


                        <button type="submit"
                            class="w-full text-white bg-custom-purple hover:bg-custom-maroon focus:ring-4 focus:outline-none focus:ring-custom-purple font-medium rounded-lg text-sm px-5 py-2.5 text-center">Login
                        </button>
                        <div class="text-sm font-medium text-gray-500 dark:text-gray-300">
                            Belum Terdaftar? <button type="button" class="text-custom-yellow hover:underline "
                                data-modal-target="signup-modal" data-modal-toggle="signup-modal">Daftar Akun</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal Daftar Akun --}}
    <div id="signup-modal" data-modal-backdrop="static" tabindex="-1" aria-hidden="true"
        class="hidden overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 justify-center items-center w-full md:inset-0 h-[calc(100%-1rem)] max-h-full">
        <div class="relative p-4 w-full max-w-2xl max-h-full">
            <!-- Modal content -->
            <div class="relative bg-white rounded-lg shadow">
                <!-- Modal header -->
                <div class="flex items-center justify-between p-4 md:p-5 border-b border-custom-purple rounded-t ">
                    <h3 class="text-lg font-semibold text-gray-900 dark:text-white">
                        Daftar Akun Baru
                    </h3>
                    <button type="button"
                        class="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 ms-auto inline-flex justify-center items-center"
                        data-modal-toggle="signup-modal">
                        <svg class="w-3 h-3" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none"
                            viewBox="0 0 14 14">
                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6" />
                        </svg>
                        <span class="sr-only">Close modal</span>
                    </button>
                </div>
                <!-- Modal body -->
                <form id="register-form" method="POST" class="p-4 md:p-5">
                    @csrf
                    <div class="grid gap-4 mb-4 grid-cols-2">
                        <div class="col-span-2">
                            <label for="nama_user"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Nama Lengkap</label>
                            <input type="text" name="nama_user" id="nama_user"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-custom-purple focus:border-custom-purple block w-full p-2.5"
                                placeholder="Masukan Nama Anda" required>
                        </div>

                        <div class="col-span-2">
                            <label for="email_user"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Email</label>
                            <input type="email_user" name="email_user" id="email_user_register"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-custom-purple focus:border-custom-purple block w-full p-2.5"
                                placeholder="Masukan Email Anda" required>
                        </div>

                        <div class="col-span-2">
                            <label for="password"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Password</label>
                            <input type="password" name="password" id="password_register"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-custom-purple focus:border-custom-purple block w-full p-2.5"
                                placeholder="Masukan Password Anda" required minlength="8">
                        </div>
                        <div class="col-span-2">
                            <label for="password_confirmation"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Konfirmasi
                                Password</label>
                            <input type="password" name="password_confirmation" id="konfirmasi_password_register"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-custom-purple focus:border-custom-purple block w-full p-2.5"
                                placeholder="Masukan Password Anda" required minlength="8">
                            <div id="password-error" class="text-red-500 text-sm mt-2"></div>
                        </div>

                        <div class="col-span-2 sm:col-span-1">
                            <label for="kecamatan_user"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Kecamatan</label>
                            <input type="text" name="kecamatan_user" id="kecamatan_user"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-custom-purple focus:border-custom-purple block w-full p-2.5"
                                placeholder="Masukan Kecamatan Anda" required>
                        </div>
                        <div class="col-span-2 sm:col-span-1">
                            <label for="kelurahan_user"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Kelurahan</label>
                            <input type="text" name="kelurahan_user" id="kelurahan_user"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-custom-purple focus:border-custom-purple block w-full p-2.5"
                                placeholder="Masukan Kelurahan Anda" required>
                        </div>

                        <div class="col-span-2">
                            <label for="detail_alamat_user"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Alamat Detail</label>
                            <input type="text" name="detail_alamat_user" id="detail_alamat_user"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-custom-purple focus:border-custom-purple block w-full p-2.5"
                                placeholder="Masukan Detail Alamat Lengkap Anda" required>
                        </div>

                        <div class="col-span-2 sm:col-span-1">
                            <label for="kode_pos_user"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Kodepos</label>
                            <input type="text" name="kode_pos_user" id="kode_pos_user"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-custom-purple focus:border-custom-purple block w-full p-2.5"
                                placeholder="Masukan Kode Pos Anda" required pattern="[0-9]+">
                        </div>
                        <div class="col-span-2 sm:col-span-1">
                            <label for="no_telp_user"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Nomor Telepon</label>
                            <input type="text" name="no_telp_user" id="no_telp_user"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-custom-purple focus:border-custom-purple block w-full p-2.5"
                                placeholder="Masukan Nomor Telepon Anda" required pattern="[0-9]+">
                        </div>

                    </div>
                    <button type="submit"
                        class="text-white w-full inline-flex items-center justify-center bg-custom-purple hover:bg-custom-midnight focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5">
                        <span class="text-center">Daftar Akun Baru</span>
                    </button>
                </form>
            </div>
        </div>
    </div>

    <!-- Your existing HTML content here -->
@endsection
