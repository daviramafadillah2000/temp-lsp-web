<?php

use App\Http\Controllers\AccountUserController;
use App\Http\Controllers\BadgeUserController;
use App\Http\Controllers\ForumCategoriesController;
use App\Http\Controllers\Quiz2Controller;
use App\Http\Controllers\ReportForumController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RankGamifikasiController;
use App\Http\Controllers\BrandController;
use App\Http\Controllers\JenisPakaianController;
use App\Http\Controllers\ProdukController;
use App\Http\Controllers\ArtikelController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\DashboardAdmin;
use App\Http\Controllers\DashboardAdminController;
use App\Http\Controllers\DashboardPelangganController;
use App\Http\Controllers\PenjualanController;
use App\Http\Controllers\PengajuanController;
use App\Http\Controllers\ForumController;
use App\Http\Controllers\ForumResponseController;
use App\Http\Controllers\inspeksiController;
use App\Http\Controllers\InteraksiAvatarController;
use App\Http\Controllers\Quiz1Controller;
use App\Http\Middleware\AdminOnly;
use App\Http\Middleware\AuthenticateUser;
use App\Http\Middleware\PelangganOnly;
use App\Http\Controllers\ReportForumResponseController;
use App\Mail\SendEmail;
use App\Models\AccountUser;
use App\Http\Middleware\InspektorOnly;
use App\Models\ReportForum;
use Illuminate\Support\Facades\Mail;

Route::view('/', 'index')->name('home');

Route::post('/register', [AccountUserController::class, 'register'])->name('account.register');


// ROUTES UNTUK PAGE YANG BUTUH MIDDLEWARE USER HARUS LOGIN KELOMPOKKAN DIBAWAH SINI
Route::middleware([AuthenticateUser::class])->group(function () {
});
// ROUTES UNTUK PAGE YANG BUTUH MIDDLEWARE USER HARUS LOGIN KELOMPOKKAN DIATAS SINI



// ROUTES UNTUK PAGE YANG BUTUH MIDDLEWARE AKUN USER HARUS ROLE ADMIN KELOMPOKKAN DIBAWAH SINI
Route::middleware([AdminOnly::class])->group(function () {
});
// ROUTES UNTUK PAGE YANG BUTUH MIDDLEWARE AKUN USER HARUS ROLE ADMIN KELOMPOKKAN DIATAS SINI
